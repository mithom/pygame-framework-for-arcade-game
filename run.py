"""
in this file, the game will be initiated and the game loop will be started. Setting will be applied here.
"""
import settings.gameSettings
import Game
import pygame
pygame.init()

setting = settings.gameSettings
game = Game.Game()

game.execute()
