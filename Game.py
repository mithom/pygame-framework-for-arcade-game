"""
this file will describe the mainloop, the gameSettings and regulate the fps (aimed at 60).
This file will also choose/change the lvl's (scenes, but might be multiple scenes/lvl
"""
import time
import pygame
import settings.gameSettings
import sys


class Game:
    """
    the game, this class will coordinate everything that happens, execute the mainloop, keep track of objects that have
    to stay alive over different scenes
    """

    def __init__(self):
        self.current_scene = None
        self.global_objects = []
        self.screen = Game.get_screen()
        self.drawn_rects = []
        self.cleared_rects = []

    def execute(self):
        """
        start the game
        :return: None
        """
        while self.is_next_scene_available():
            "scene need to be initialised, run and closed"
            self.current_scene = self.get_next_scene()
            self.current_scene.draw_background()
            try:
                "game loop"
                while not self.current_scene.is_finished():
                    for event in pygame.event.get():
                        if event.type in (pygame.QUIT, pygame.KEYDOWN):
                            sys.exit()

                    starting_time = time.clock()
                    self.game_loop()
                    pygame.display.update(self.drawn_rects + self.cleared_rects)

                    passed_time = time.clock() - starting_time
                    if passed_time < 1.0/settings.gameSettings.max_fps:
                        pygame.time.delay(int(1000*1.0/settings.gameSettings.max_fps-passed_time))
                    else:
                        pass  # TODO: warn that everything is going too slow
                    print "time passed: " + str(passed_time)
                    print "updated rects:", self.drawn_rects
            finally:
                self.current_scene.close_scene()

    @staticmethod
    def is_next_scene_available():
        """
        returns if there is a next scene available or not
        :return: boolean    | is there a next scene
        """
        return len(settings.gameSettings.all_levels) > 0

    def get_next_scene(self):
        return settings.gameSettings.all_levels.pop(0)(self)

    @staticmethod
    def get_screen():
        return pygame.display.set_mode((settings.gameSettings.screen_width, settings.gameSettings.screen_height))

    def game_loop(self):
        self.clear_background()
        self.update_positions()
        self.draw_objects()

    def clear_background(self):
        for rect in self.drawn_rects:
            print rect
            self.screen.blit(self.current_scene.get_background(), rect, rect)
        self.cleared_rects = self.drawn_rects[:]
        self.drawn_rects = []

    def update_positions(self):
        for gameObject in self.get_all_objects():
            gameObject.update(self)

    def draw_objects(self):
        for game_object in self.get_all_objects():
            game_object.draw(self.screen)
            self.drawn_rects.append(game_object.pos)

    def get_all_objects(self):
        return self.global_objects + self.current_scene.objects
