"""
this should describe how a scene should look like a java interface, check if something like that exists in python.
"""
import pygame
import settings.gameSettings
import gameObjects.gameObjects


class SceneInterface:
    def __init__(self, game):
        self.game = game
        self.objects = [gameObjects.gameObjects.GamObject("player.jpg")]
        self.background = pygame.Surface((30, 40))

    def get_background(self):
        return self.background

    def is_finished(self):
        return True

    def close_scene(self):
        self.game.screen.fill(settings.gameSettings.background_color)
        self.game.current_scene = None

    def draw_background(self):
        self.game.screen.fill(settings.gameSettings.background_color)
        self.game.screen.blit(self.background, (0, 0))
        self.game.drawn_rects.append(self.game.screen.get_rect())


class Scene1(SceneInterface):

    def __init__(self, game):
        SceneInterface.__init__(self, game)
        self.background = pygame.transform.scale(self.load_background_image(), (settings.gameSettings.screen_width, settings.gameSettings.screen_height))

    def load_background_image(self):
        return pygame.image.load("background.jpg").convert()

    def is_finished(self):
        return False