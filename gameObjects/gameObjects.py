import pygame
import random


class GamObject:

    def __init__(self, image):
        self.image = pygame.image.load(image).convert()
        self.pos = self.image.get_rect()

    def update(self, game):
        self.pos = self.pos.move(random.randint(0,5),random.randint(0,5))

    def draw(self, screen):
        screen.blit(self.image, self.pos)
