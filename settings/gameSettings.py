"""
this file will describe all gameSettings, so they aren't hardcoded and can be changed easily.
gameSettings are the general settings, if they apply to a specific object, they shall be specified inside the object or
an object settings file
"""
import scenes.scene_interface

all_levels = [scenes.scene_interface.Scene1]

max_fps = 30.0
# TODO: make these data's fullscreen (with sys module)
screen_width = 480
screen_height = 360
background_color = 0, 0, 255
